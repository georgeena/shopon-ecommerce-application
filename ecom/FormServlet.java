import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.*;

public class FormServlet extends HttpServlet
{
     
public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException
	{
		resp.setContentType("text/html");
  PrintWriter out= resp.getWriter();
  String title="Thanks for submitting the form";
  
  out.println("<html>");
  out.println("<head><title>ViewForm</title></head>");
  
  out.println("<body bgcolor=\"pink\">");
  out.println("<H1 ALIGN=\"CENTER\">" + title + "</H1>\n");
  out.println("<table BORDER=1 align=\"center\">\n"); 
 out.println("<tr BGCOLOR=\"pink\">\n"); 
 out.println("<th>Parameter Name</th><th>Parameter Value</th>");
 Enumeration names = req.getParameterNames();
 while(names.hasMoreElements()) {
 String paramName = (String)names.nextElement();
 out.println("<tr><td>" + paramName);
 out.println(" <td>" + req.getParameter(paramName));
 }
 out.println("</table>\n</body></html>");
	}
}